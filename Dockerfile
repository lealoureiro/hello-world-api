FROM golang:1.23.2-alpine3.20 AS builder

LABEL maintainer="Leandro Loureiro <leandroloureiro@pm.me>"

ARG APP_VERSION=dev

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY config config
COPY hello-world-api.go .

RUN CGO_ENABLED=0 GOOS=linux \
    go build -a -ldflags "-X 'github.com/lealoureiro/hello-world-api/config.Version=${APP_VERSION}'" \
    -installsuffix cgo -o /go/bin/hello-world-api .

FROM alpine:3.20

RUN apk --no-cache add curl

WORKDIR /

COPY --from=builder /go/bin/hello-world-api .

ENV GIN_MODE=release

EXPOSE 8080

HEALTHCHECK --start-period=1s --interval=3600s --timeout=1s --retries=1 \
    CMD curl --silent --fail http://localhost:8080/health || exit 1

ENTRYPOINT ["/hello-world-api"]